//
//  Storage.swift
//  FloatingWidget
//
//  Created by Erez Haim on 9/1/15.
//  Copyright (c) 2015 fortvision. All rights reserved.
//

import Foundation
class Storage {
    
    private let USER_ID_KEY = "fortvision_widget_SDK_user_id_key"
    private var userId: String!
    static let sharedInstance = Storage()
    
    init() {
        let defaultUser = NSUserDefaults.standardUserDefaults()
        if let user = defaultUser.objectForKey(USER_ID_KEY) as? String{
            userId = user
        } else {
            let UUID = CFUUIDCreate(nil)
            let UUIDStr = CFUUIDCreateString(nil, UUID)
            NSUserDefaults.standardUserDefaults().setObject(UUIDStr, forKey: USER_ID_KEY)
            NSUserDefaults.standardUserDefaults().synchronize()
            userId = String(UUIDStr)
        }
    }
    
    func getUserID() ->String{
        return userId
    }

}