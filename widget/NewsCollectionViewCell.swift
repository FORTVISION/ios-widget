//
//  NewsCollectionViewCell.swift
//  test-widget-swift
//
//  Created by Erez Haim on 9/7/15.
//  Copyright (c) 2015 fortvision. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var label6: UILabel!
    
    
    var ids: [Dictionary<String, AnyObject>] = []
    
    @IBAction func imageWasTouched(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "ImageWasChosen", object: sender))
    }

    
}
