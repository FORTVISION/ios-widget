//
//  TodayViewController.swift
//  widget
//
//  Created by Erez Haim on 9/7/15.
//  Copyright (c) 2015 fortvision. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var items: Array<Dictionary<String, AnyObject>> = []
    var itemCount = 0
    
    let publisher_id = 11
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var wrapperView: UIView!
    
    override func loadView() {
        super.loadView()
        getTableData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UILabel.appearance().textAlignment = .Right
        UIImageView.appearance().contentMode = .ScaleAspectFill

        self.preferredContentSize = CGSizeMake(320, 240)
        wrapperView.layer.cornerRadius = 8
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "imageWasChosen:", name: "ImageWasChosen", object: nil)
        // Do any additional setup after loading the view from its nib.
    }
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.items.count > 0
        {
            sendWidgetShowedEvent()
        }
    }
    
    func sendWidgetShowedEvent ()
    {
        if collectionView.visibleCells().count > 0
        {
            let cell = collectionView.visibleCells() .first as! NewsCollectionViewCell
            RemoteAPI().sendWidgetShowedClick(cell.ids , publisherId: self.publisher_id)
        }
        
    }
    
    
    func imageWasChosen(notification: NSNotification){
        if let button = notification.object as? UIButton{
            if let link = items[button.tag]["link"] as? String {
                print("Link \(link)")
                if let itemId = items[button.tag]["id"] as? NSNumber
                {
                    RemoteAPI().sendArticleClick(itemId.integerValue, publisherId: publisher_id)
                }
                self.extensionContext?.openURL(NSURL(string: link)!, completionHandler: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.NewData)
    }
    
    func getTableData(){
        let userId = Storage.sharedInstance.getUserID()
        let urlPath = "http://extension.fortvision.com/items/get?publisher_id=\(publisher_id)&max_impressions=1&user_id=\(userId))"
        let url: NSURL = NSURL(string: urlPath)!
        activityIndicatorView.startAnimating()
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) {(data, response, error) in
            
            if let receivedData = data {
                do {
                    let JSONResponse = try NSJSONSerialization.JSONObjectWithData(receivedData, options: NSJSONReadingOptions.MutableContainers) as! Array<Dictionary<String, AnyObject>>
                    
                    self.items = JSONResponse
                    if self.items.count > 0
                    {
                        self.sendWidgetShowedEvent()
                    }
                    self.handleData({ (success) -> Void in
                        dispatch_async(dispatch_get_main_queue()){
                            self.collectionView.reloadData()
                            self.activityIndicatorView.stopAnimating()
                        }
                    })
                } catch let error {
    
                    RemoteAPI().sendError( String (error), publisherId: self.publisher_id)
                    
                }
            }
        }
        
        task.resume()
    }
    
    func preloadImage(index: Int, completion: ((success: Bool) -> Void)){
        if let url = items[index]["small_img_link"] as? String {
            getDataFromUrl(NSURL(string: url)!, completion: { (data) -> Void in
                self.items[index]["image"] = UIImage(data: data!)
                completion(success: true)
            })
        }
    }
    
    func getDataFromUrl(url:NSURL, completion: ((data: NSData?) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            completion(data: data)
            }.resume()
    }
    
    func handleData(completion: ((success: Bool) -> Void)){
        var counter = 0
        for index in 0..<items.count {
            if let date = dateFromDateStr(items[index]["pubdate"] as? String){
                items[index]["date"] = date
            }
            preloadImage(index, completion: { (success) -> Void in
                counter++
                if counter == self.items.count {
                    completion(success: true)
                }
            })
        }
    }
    
    func dateFromDateStr(dateStr: String?) -> NSDate?{
        if dateStr != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC");
            dateFormatter.dateFormat = NSDateFormatter.dateFormatFromTemplate("yyyy-MM-dd'T'HH:mm:ss", options: 0, locale: NSLocale.currentLocale())
            var date = dateFormatter.dateFromString(dateStr!)
            if date == nil {
                var str = dateStr!.stringByReplacingOccurrencesOfString("T", withString: " ")
                str = str.stringByReplacingOccurrencesOfString("Z", withString: "")
                str = str.stringByPaddingToLength(19, withString: "", startingAtIndex: 0)
                date = dateFormatter.dateFromString(str)
            }
            return date
        }
        return nil
    }
    
    @IBAction func moveLeft(sender: UIButton) {
        if collectionView.visibleCells().count == 0
        {
            return
        }
        itemCount--
        if itemCount < 0 {
            itemCount = 0
        }
        collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: itemCount, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
        
    }
    
    @IBAction func moveRight(sender: UIButton) {
        if collectionView.visibleCells().count == 0
        {
            return
        }
        itemCount++
        if itemCount >= items.count/6 - 1 {
            itemCount = items.count/6 - 1
        }
        
        collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: itemCount, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
    }
    
    @IBAction func openTitleLink(sender: AnyObject) {
        self.extensionContext?.openURL(NSURL(string: "http://www.sport5.co.il/")!, completionHandler: nil)
    }
    
    @IBAction func openFooterLink(sender: AnyObject) {
        self.extensionContext?.openURL(NSURL(string: "http://www.fortvision.com/")!, completionHandler: nil)
    }
    
    // MARK - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            return CGSizeMake(self.collectionView.frame.width+20, 180)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count/6
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("NewsCell", forIndexPath: indexPath) as! NewsCollectionViewCell
        
        let buttonArray = [cell.button1, cell.button2, cell.button3, cell.button4, cell.button5, cell.button6]
        let labelArray = [cell.label1, cell.label2, cell.label3, cell.label4, cell.label5, cell.label6]
        
        let interval = indexPath.row * 6
        
        cell.button1.tag = interval
        cell.button2.tag = interval + 1
        cell.button3.tag = interval + 2
        cell.button4.tag = interval + 3
        cell.button5.tag = interval + 4
        cell.button6.tag = interval + 5
        
        for var i = interval; i < interval + 6; i++ {
            
            let item = items[i]
            let elementId = item["id"] as? NSNumber
            cell.ids.append(["article_id":elementId!])
            labelArray[i-interval].text = item["title"] as? String
            buttonArray[i-interval].setBackgroundImage(item["image"] as? UIImage, forState: UIControlState.Normal)
        }
        return cell
        
    }
    
    
    
}
