//
//  RemoteAPI.swift
//  FloatingWidget
//
//  Created by Erez Haim on 9/1/15.
//  Copyright (c) 2015 fortvision. All rights reserved.
//

import Foundation

class RemoteAPI {
    
    private let REMOTE_URL = "http://extension.fortvision.com/sdkevents/post"
    static let sharedInstance = RemoteAPI()
    
    init() {
        
    }
    
    func sendArticleClick(id: Int, publisherId: Int){
        post(["publisher_id": String ( publisherId ),
            "device": "iPhone",
            "advertising_id": Storage.sharedInstance.getUserID(),
            "event_id": 3,
            "data": id], url: REMOTE_URL)
    }
    
    func sendWidgetShowedClick(idsArray: [Dictionary<String, AnyObject>], publisherId: Int){
        post(["publisher_id": String( publisherId),
            "device": "iPhone",
            "advertising_id": Storage.sharedInstance.getUserID(),
            "event_id": 2,
            "data": idsArray], url: REMOTE_URL)
    }
    
    func sendError(dataError: String, publisherId: AnyObject){
        post(["publisher_id": String(publisherId),
            "device": "iPhone",
            "event_id": 1,
            "data": dataError], url: REMOTE_URL)
    }
    
    func post(params : Dictionary<String, AnyObject>, url : String) {
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "POST"
        
        do {
            let bodyData =  try NSJSONSerialization.dataWithJSONObject(params, options: .PrettyPrinted)
            request.HTTPBody = bodyData
        } catch let error {

            print("Json parsing error \(error)")
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            
            let res = response as! NSHTTPURLResponse
            if res.statusCode != 200
            {
                let publisherId = params["publisher_id"]
                self.sendError(String(response), publisherId:publisherId!)
                return
            }
            
            print("Response: \(response)")
        })
        
        task.resume()
    }
}